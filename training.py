import torch.optim as optim
import torch.nn as nn
from TomatoModel import net
learning_rate = 0.1

optimizer = optim.SGD(net.parameters(), lr=learning_rate, momentum=0.9)
criterion = nn.BCEWithLogitsLoss()

scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.5)

for epoch in range(10):  # loop over the dataset multiple times

    scheduler.step()

    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data


        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs.cpu(), labels)

        loss.backward()
        optimizer.step()


        lossl.append(loss.item())

        # print statistics
        running_loss += loss.item()
        if i % 4 == 0:    # print every 2000 mini-batches
            print("{} loss: {} ".format(epoch + 1,  running_loss ))
            running_loss = 0.0

print('Finished Training')
