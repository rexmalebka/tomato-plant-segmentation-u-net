import torch
import torch.nn as nn
import torch.nn.functional as F


# device = torch.device('cuda')

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.pool = nn.MaxPool2d((2, 2), stride=(2, 2))
        
        self.conv1 = nn.Conv2d(3, 64, 3)
        self.conv2 = nn.Conv2d(64, 64, 3)
        self.bn1 = nn.BatchNorm2d(64)
        
        self.conv3 = nn.Conv2d(64, 128, 3)
        self.conv4 = nn.Conv2d(128, 128, 3)
        self.bn2 = nn.BatchNorm2d(128)
        
        self.conv5 = nn.Conv2d(128, 256, 3)
        self.conv6 = nn.Conv2d(256, 256, 3)
        self.bn3 = nn.BatchNorm2d(256)
        
        self.conv7 = nn.Conv2d(256, 512, 3)
        self.conv8 = nn.Conv2d(512, 512, 3)
        self.bn4 = nn.BatchNorm2d(512)
        
        self.conv9 = nn.Conv2d(512, 1024, 3)
        self.conv10 = nn.Conv2d(1024, 1024, 3)
        self.bn5 = nn.BatchNorm2d(1024)
        
        self.conv11 = nn.Conv2d(64, 3, 1)
        
        self.upconv1 = nn.Conv2d(1024, 512, 3)
        self.upconv2 = nn.Conv2d(512, 256, 3)
        self.upconv3 = nn.Conv2d(256, 128, 3)
        self.upconv4 = nn.Conv2d(128, 64, 3)
        self.upconv5 = nn.Conv2d(64, 2, 1)

    def forward(self, x):
        # x = x.to(device)
        # B 3 572 572
        x = nn.LeakyReLU()(
            self.bn1(self.conv1(x))
        )  
        # B 64 570 570
        x = nn.LeakyReLU()(
            self.bn1(self.conv2(x))
        )   
        crop4 = x[:, :, 285-196:285+196, 285-196:285+196]
        # B 64 568 568
        x = self.pool(x)            
        # B 64 284 284
                       
        # B 64 284 284
        x = nn.LeakyReLU()(
            self.bn2(self.conv3(x))
        )
        # B 128 282 282
        x = nn.LeakyReLU()(
            self.bn2(self.conv4(x))
        ) 
        crop3 = x[:, :, 140-100:140+100, 140-100:140+100]
        # B 128 280 280        
        x = self.pool(x)
        # B 128 140 140  
        
        # B 128 140 140
        x = nn.LeakyReLU()(
            self.bn3(self.conv5(x))
        ) 
        # B 256 138 138        
        x = nn.LeakyReLU()(
            self.bn3(self.conv6(x))
        ) 
        # B 256 136 136        
        crop2 = x[:, :, 68-52:68+52, 68-52:68+52]
        x = self.pool(x)        
        # B 256 68 68
        
        # B 256 68 68
        x = nn.LeakyReLU()(
            self.bn4(self.conv7(x))
        ) 
        # B 512 66 66        
        x = nn.LeakyReLU()(
            self.bn4(self.conv8(x))
        ) 
        # B 512 64 64
        crop1 = x[:, :, 32-28:32+28, 32-28:32+28]
        x = self.pool(x)
        # B 512 32 32   
        
        # B 512 32 32
        x = nn.LeakyReLU()(
            self.bn5(self.conv9(x))
        )  
        # B 1024 30 30        
        x = nn.LeakyReLU()(
            self.bn5(self.conv10(x))
        )  
        # B 1024 28 28
        x5 = x
        
        # expansor
        
        # B 1024 28 28
        x = F.interpolate(x5, scale_factor=2, mode='bilinear')
        x = x[:, 0:512, :, :]
        
        # B 512 56 56  +  B 512 56 56
        up1 = torch.cat((crop1, x), 1)              
        x = nn.LeakyReLU()(up1)
        
        # B 1024 56 56
        x = nn.LeakyReLU()(self.upconv1(x))
        # B 512 54 54
        x4 = nn.LeakyReLU()(self.conv8(x))
        
        # B 512 52 52
        x = F.interpolate(x4, scale_factor=2, mode='bilinear')
        x = x[:, 0:256, :, :]
        
        # B 256 104 104  +  B 256 104 104
        up2 = torch.cat((crop2, x), 1)              
        x = nn.LeakyReLU()(up2)
        
        # B 512 104 104
        x = nn.LeakyReLU()(self.upconv2(x))
        # B 256 102 102
        x3 = nn.LeakyReLU()(self.conv6(x))
               
        # B 256 100 100
        x = F.interpolate(x3, scale_factor=2, mode='bilinear')
        x = x[:, 0:128, :, :]
        
        # B 128 200 200 + 128 200 200
        up3 = torch.cat((crop3, x), 1)  
        x = nn.LeakyReLU()(up3)
                
        # B 256 200 200
        x = nn.LeakyReLU()(self.upconv3(x))
        # B 128 198 198
        x4 = nn.LeakyReLU()(self.conv4(x))      
        
        # B 128 196 196
        x = F.interpolate(x4, scale_factor=2, mode='bilinear')
        x = x[:, 0:64, :, :]
        
        # B 64 196 196 + 64 196 196
        up3 = torch.cat((crop4, x), 1)  
        x = nn.LeakyReLU()(up3)
                
        # B 128 392 392
        x = nn.LeakyReLU()(self.upconv4(x))
        # B 128 390 390
        x4 = nn.LeakyReLU()(self.conv2(x))  
        # B 128 388 388
        x = self.conv11(x4)
        x = nn.Softmax(dim=1)(x)

        return x


net = Net() # .to(device)
