# Tomato Plant Segmentation

Implementation of U-Net Architecture for tomato plant segmentation.

## Overview

Using my own labeled dataset [Tomato Plant Dataset](https://gitlab.com/rexmalebka/tomato-plant-dataset) 

## Architecture Modifications

* Changes in the input shape of layers for acepting RGB Images.
* Changes in the output shape for the three classification layers (folliage, tomato, background).
* Changed ReLu for LeakyReLU, smoothing loss function
* Added Batch Normalization, improving learning time.

## Training 

Used for trainig Binary Cross Entropy with logits loss function and Stochastic Gradient Descendent optimizer in 15 epochs with lr=0.1.

![Loss](loss.png "Loss")

## Testing 

Used mean Intersection over Union for testing, with 66.96% on 15 epochs.

![samples](samples.png "Samples")
![labels](labels.png "labels")
![prediction](prediction.png "prediction")

* Pytorch 1.1.0
* Torchvision 0.3.0
* Skimage (from scipy) 1.3.0
* PIL 4.3.0
* Numpy 1.16.4

